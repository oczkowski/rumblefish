package behaviour;

import org.openqa.selenium.WebDriver;

/**
 * Created by kuba on 10/11/18.
 */
public class Given {

    private WebDriver driver;

    public Given(WebDriver driver) {
        this.driver = driver;
    }

    public Given pageLoaded(String url) {
        driver.get(url);
        return this;
    }
}
