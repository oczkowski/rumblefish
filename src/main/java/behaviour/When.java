package behaviour;

import elements.SecondPostDownArrowButton;
import elements.SecondPostUpArrowButton;
import org.openqa.selenium.*;
import pages.LogInComponent;
import pages.SearchResultsPage;
import pages.SubRedditPage;
import pages.SuperComponent;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/11/18.
 */
public class When {

    private WebDriver driver;
    private SuperComponent superComponent;
    private LogInComponent logInComponent;

    public When(WebDriver driver) {
        this.driver = driver;
    }

    public When searchForSubredditPage(String subredditName) {
        superComponent = new SuperComponent(driver);
        superComponent.enterSearchKeyword(subredditName);
        return this;
    }

    public When openLocalizedSubredditPage(String subredditName) {
        new SearchResultsPage(driver).openChosenSubredditPage(subredditName);
        new SubRedditPage(driver, subredditName);
        return this;
    }

    public When printOutTopMostTitle() {
        System.out.println(driver.findElements(By.cssSelector("a[href*='comments'] > h2")).get(0).getText());
        return this;
    }

    public When loginAsUser(String username, String password) {
        superComponent.logIn();
        logInComponent = new LogInComponent(driver);
        logInComponent
                .enterUserName(username)
                .enterPassword(password)
                .clickSignInButton()
                .closeLoginComponent();

        //it has to be repeated as Reddit Login process is broken
        superComponent.logIn();
        WaitForConditions.waitForElementToBeNotDisplayed(driver, By.xpath("//div[button]//iframe"));
        return this;
    }

    public When voteToTheSecondPost() {

        String downVoteLocator = "(//div[contains(@class, 'scrollerItem')]/div/div/button[@data-click-id='downvote'])[2]";

        WebElement secondUpvote = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, driver.findElement(By.xpath(downVoteLocator)));
        WaitForConditions.waitForToBeNonStale(driver, secondUpvote);

        if (secondUpvote.getAttribute("aria-pressed").equals("true")) {
            new SecondPostDownArrowButton(driver).clickIt();
        }
        else {
            new SecondPostUpArrowButton(driver).clickIt();
        }
        return this;
    }
}
