package behaviour;

import elements.SecondPostDownArrowButton;
import elements.SecondPostUpArrowButton;
import org.openqa.selenium.WebDriver;
import static org.assertj.core.api.Assertions.*;

/**
 * Created by kuba on 10/11/18.
 */
public class Then {

    private WebDriver driver;

    public Then(WebDriver driver) {
        this.driver = driver;
    }

    public Then upOrDownArrowButtonShouldBeChosen() {
        assertThat(new SecondPostUpArrowButton(driver).beenHasChosen() || new SecondPostDownArrowButton(driver).beenHasChosen()).isTrue();
        return this;
    }
}
