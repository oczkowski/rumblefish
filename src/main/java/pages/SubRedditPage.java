package pages;

import elements.CommunityDetails;
import elements.SubRedditPageHeader;
import elements.SubRedditPost;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

/**
 * Created by kuba on 10/9/18.
 */
public class SubRedditPage {

    private WebDriver driver;

    private CommunityDetails communityDetails;
    private SubRedditPageHeader subRedditPageHeader;
    private ArrayList<SubRedditPost> subRedditPosts;

    public SubRedditPage(WebDriver driver, String subRedditPageName) {
        this.driver = driver;
        communityDetails = new CommunityDetails(driver);
        subRedditPageHeader = new SubRedditPageHeader(driver, subRedditPageName);
    }
}
