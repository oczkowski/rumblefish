package pages;

import elements.*;
import org.openqa.selenium.WebDriver;

/**
 * Created by kuba on 10/7/18.
 */
public class MainPage {

    private WebDriver driver;

    private Popular popular;
    private TrendingCommunities trendingCommunities;

    public MainPage(WebDriver driver) {
        this.driver = driver;

        popular = new Popular(driver);
        trendingCommunities = new TrendingCommunities(driver);
    }
}
