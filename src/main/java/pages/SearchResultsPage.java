package pages;

import elements.CommunitiesAndUsersElement;
import org.openqa.selenium.WebDriver;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class SearchResultsPage {

    private WebDriver driver;

    public SearchResultsPage(WebDriver driver) {
        this.driver = driver;
        WaitForConditions.waitForPageToBeLoaded(driver);
    }

    public SearchResultsPage openChosenSubredditPage(String subredditName) {
        new CommunitiesAndUsersElement(driver, subredditName).clickIt();
        return this;
    }
}
