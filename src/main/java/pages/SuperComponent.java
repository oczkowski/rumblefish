package pages;

import elements.*;
import org.openqa.selenium.WebDriver;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class SuperComponent {

    private WebDriver driver;

    private MainBar mainBar;
    private LogInButton logInButton;
    private SignUpButton signUpButton;
    private SearchInput searchInput;

    public SuperComponent(WebDriver driver) {
        this.driver = driver;
        WaitForConditions.waitForPageToBeLoaded(driver);
        mainBar = new MainBar(driver);
        logInButton = new LogInButton(driver);
        signUpButton = new SignUpButton(driver);
        searchInput = new SearchInput(driver);
    }

    public SuperComponent enterSearchKeyword(String keyword) {
        searchInput
                .enterText(keyword)
                .confirm();
        return this;
    }

    public SuperComponent logIn() {
        logInButton.clickIt();
        return this;
    }
}
