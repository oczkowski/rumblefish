package pages;

import elements.LogInComponentCloseButton;
import elements.LogInPasswordInput;
import elements.LogInSignInButton;
import elements.LogInUsernameInput;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by kuba on 10/9/18.
 */
public class LogInComponent {

    private WebDriver driver;

    public LogInComponent(WebDriver driver) {
        this.driver = driver;
    }

    public LogInComponent enterUserName(String username) {
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[src=\"https://www.reddit.com/login\"]")));
        new LogInUsernameInput(driver).enterText(username);
        driver.switchTo().defaultContent();
        return this;
    }

    public LogInComponent enterPassword(String password) {
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[src=\"https://www.reddit.com/login\"]")));
        new LogInPasswordInput(driver).enterText(password);
        driver.switchTo().defaultContent();
        return this;
    }

    public LogInComponent clickSignInButton() {
        driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[src=\"https://www.reddit.com/login\"]")));
        new LogInSignInButton(driver).clickIt();
        driver.switchTo().defaultContent();
        return this;
    }

    public LogInComponent closeLoginComponent() {
        new LogInComponentCloseButton(driver).clickIt();
        return this;
    }
}
