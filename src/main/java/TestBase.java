import behaviour.Given;
import behaviour.Then;
import behaviour.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;


/**
 * Created by kuba on 10/7/18.
 */
public class TestBase {

    protected WebDriver driver;

    protected Given given;
    protected When when;
    protected Then then;

    @BeforeTest(alwaysRun = true)
    public void init() {
        FirefoxOptions options = new FirefoxOptions();
        options.setCapability("marionette", false);
        driver = new FirefoxDriver(options);
        given = new Given(driver);
        when = new When(driver);
        then = new Then(driver);
    }

    @AfterTest(alwaysRun = true)
    public void closeIt() {
        driver.quit();
    }
}
