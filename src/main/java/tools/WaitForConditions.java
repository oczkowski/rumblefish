package tools;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;
import java.util.concurrent.TimeUnit;
import static org.testng.AssertJUnit.fail;

/**
 * Created by kuba on 10/7/18.
 */
public class WaitForConditions {

    public static WebElement waitForElementToBePresentAndDisplayed(WebDriver driver, WebElement webElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.visibilityOf(webElement));
    }

    public static WebElement waitForElementToBePresentAndDisplayed(WebDriver driver, By selector) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(selector));
    }

    public static void waitForElementToBeNotDisplayed(WebDriver driver, By selector) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(15, TimeUnit.SECONDS)
                .pollingEvery(100, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
    }

    public static Boolean hasAttributeChanged(WebDriver driver, WebElement element, String attributeName, String expectedValue) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        return wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {

                String value = element.getAttribute(attributeName);
                if (value.equals(expectedValue))
                    return true;
                else return false;
            }
        });
    }

    public static void waitForToBeNonStale(WebDriver driver, WebElement element) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(10, TimeUnit.SECONDS)
                .pollingEvery(1, TimeUnit.SECONDS)
                .ignoring(StaleElementReferenceException.class);
        wait.until(ExpectedConditions.not(ExpectedConditions.stalenessOf(element)));
    }

    public static void waitForPageToBeLoaded(WebDriver driver) {
        By htmlTag = By.tagName("html");
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .ignoring(NoSuchElementException.class);
        try{
            wait.until(ExpectedConditions.visibilityOfElementLocated(htmlTag));
        }
        catch(TimeoutException e) {
            fail("Expected page was not loaded.");
        }
    }
}
