package elements;

import elements.global.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class SignUpButton implements Button {

    private WebDriver driver;
    private WebElement element;

    public SignUpButton(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//a[text()='sign up']"));
    }

    @Override
    public SignUpButton clickIt() {
        element.click();
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
