package elements.global;

/**
 * Created by kuba on 10/7/18.
 */
public interface Link extends PageElement {
    Link clickIt();
}
