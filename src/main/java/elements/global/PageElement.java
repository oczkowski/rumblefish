package elements.global;

import org.openqa.selenium.WebElement;

/**
 * Created by kuba on 10/7/18.
 */
public interface PageElement {
    Boolean isLoaded();
}
