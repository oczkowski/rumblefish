package elements.global;

/**
 * Created by kuba on 10/7/18.
 */
public interface Button extends PageElement {
    Button clickIt();
}
