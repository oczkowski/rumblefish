package elements.global;

/**
 * Created by kuba on 10/7/18.
 */
public interface Input extends PageElement {
    Input enterText(String text);
//    Input confirm();
}
