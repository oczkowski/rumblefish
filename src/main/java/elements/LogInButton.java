package elements;

import elements.global.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class LogInButton implements Button {

    private WebDriver driver;
    private WebElement element;

    public LogInButton(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//a[text()='log in']"));
    }

    @Override
    public LogInButton clickIt() {
        element.click();
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
