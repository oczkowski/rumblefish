package elements;

import elements.global.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class MainBar implements PageElement {

    private WebDriver driver;
    private WebElement element;

    public MainBar(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.cssSelector("header"));
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
