package elements;

import elements.global.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class CommunityDetails implements PageElement {

    private WebDriver driver;
    private WebElement element;

    public CommunityDetails(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//div[div[text()='Community Details']]"));
    }

    public String getName() {
        return element.findElement(By.xpath("//a/span")).getText();
    }

    @Override
    public Boolean isLoaded() {
        return null;
    }
}
