package elements;

import elements.global.Add;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class TrendingCommunities implements Add {

    private WebDriver driver;
    private WebElement element;

    public TrendingCommunities(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//div[div[text()='Trending Communities']]"));
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
