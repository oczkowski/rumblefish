package elements;

import elements.global.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class SearchInput implements Input {

    private WebDriver driver;
    private WebElement element;

    public SearchInput(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.cssSelector("#header-search-bar"));
    }

    @Override
    public SearchInput enterText(String text) {
        element.sendKeys(text);
        return this;
    }

    public String getText() {
        return element.getText();
    }

    public SearchInput confirm() {
        element.sendKeys(Keys.RETURN);
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
