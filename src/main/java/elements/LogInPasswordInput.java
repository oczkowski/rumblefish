package elements;

import elements.global.Input;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/10/18.
 */
public class LogInPasswordInput implements Input {

    private WebDriver driver;
    private WebElement element;

    public LogInPasswordInput(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.cssSelector("#loginPassword"));
    }

    @Override
    public LogInPasswordInput enterText(String text) {
        element.sendKeys(text);
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
