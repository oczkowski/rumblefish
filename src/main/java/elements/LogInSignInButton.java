package elements;

import elements.global.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/10/18.
 */
public class LogInSignInButton implements Button {

    private WebDriver driver;
    private WebElement element;

    public LogInSignInButton(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.cssSelector(".AnimatedForm__submitButton"));
    }

    @Override
    public LogInSignInButton clickIt() {
        // normal click doesn't work at all
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", element);
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
