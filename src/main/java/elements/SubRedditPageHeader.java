package elements;

import elements.global.PageElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/9/18.
 */
public class SubRedditPageHeader implements PageElement {

    private WebDriver driver;
    private WebElement element;

    public SubRedditPageHeader(WebDriver driver, String subRedditPageName) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//span[text()='r/']/h1[text()='"+ subRedditPageName + "']"));
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
