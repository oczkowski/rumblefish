package elements;

import elements.global.Button;
import org.openqa.selenium.*;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/11/18.
 */
public class SecondPostDownArrowButton implements Button {

    private WebDriver driver;
    private String downVoteLocator = "(//div[contains(@class, 'scrollerItem')]/div/div/button[@data-click-id='downvote'])[2]";

    public SecondPostDownArrowButton(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public SecondPostDownArrowButton clickIt() {
        //indeed, this is awful, but instability of DOM structure forces it. The alternative is Thread.sleep
        WaitForConditions.waitForElementToBePresentAndDisplayed(driver, driver.findElement(By.xpath(downVoteLocator)));
        try {
            WaitForConditions.waitForToBeNonStale(driver, driver.findElement(By.xpath(downVoteLocator)));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(downVoteLocator)));
        } catch (StaleElementReferenceException e) { }

        try {
            WaitForConditions.waitForToBeNonStale(driver, driver.findElement(By.xpath(downVoteLocator)));
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(downVoteLocator)));
        } catch (StaleElementReferenceException | TimeoutException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(downVoteLocator)));
        }
        try {
            WaitForConditions.hasAttributeChanged(driver, driver.findElement(By.xpath(downVoteLocator)), "aria-pressed", "true");
        } catch (TimeoutException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(downVoteLocator)));
            WaitForConditions.hasAttributeChanged(driver, driver.findElement(By.xpath(downVoteLocator)), "aria-pressed", "true");
        }

        return this;
    }

    @Override
    public Boolean isLoaded() {
        return driver.findElement(By.xpath("(//div[contains(@class, 'scrollerItem')]/div/div/button[@data-click-id='downvote'])[2]")).isDisplayed();
    }

    public Boolean beenHasChosen() {
        return driver.findElement(By.xpath(downVoteLocator)).getAttribute("aria-pressed").equals("true");
    }
}
