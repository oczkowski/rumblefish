package elements;

import elements.global.Button;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/10/18.
 */
public class LogInComponentCloseButton implements Button {

    private WebDriver driver;
    private WebElement element;

    public LogInComponentCloseButton(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//div[iframe]/button"));
    }

    @Override
    public LogInComponentCloseButton clickIt() {
        element.click();
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
