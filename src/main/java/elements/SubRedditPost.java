package elements;

import elements.global.Post;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/9/18.
 */
public class SubRedditPost implements Post {

    private WebDriver driver;
    private WebElement element;

    private String locator = "#SHORTCUT_FOCUSABLE_DIV > div >" +
            " div:nth-of-type(2) > div > div > div > div > div:nth-of-type(2) > div:nth-of-type(3) > div:nth-of-type(1) > " +
            "div > div:nth-of-type(2) > div";

    public SubRedditPost(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.cssSelector(locator));
    }

    @Override
    public SubRedditPost clickIt() {
        element.click();
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
