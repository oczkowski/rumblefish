package elements;

import elements.global.Post;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by kuba on 10/7/18.
 */
public class RedditPost implements Post {

    private WebDriver driver;
    private WebElement element;

    public RedditPost(WebDriver driver) {
        this.driver = driver;
        element = driver.findElement(By.cssSelector("div#SHORTCUT_FOCUSABLE_DIV > div > div:nth-of-type(2) > div > div > div > " +
                "div:nth-of-type(2) > div:nth-of-type(3) > div:nth-of-type(1) > div:nth-of-type(2) > div:nth-of-type(1) > div"));
    }

    @Override
    public RedditPost clickIt() {
        element.click();
        return this;
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
