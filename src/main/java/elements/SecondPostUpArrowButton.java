package elements;

import elements.global.Button;
import org.openqa.selenium.*;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/11/18.
 */
public class SecondPostUpArrowButton implements Button{

    private WebDriver driver;
    private String upVoteLocator = "(//div[contains(@class, 'scrollerItem')]/div/div/button[@data-click-id='upvote'])[2]";

    public SecondPostUpArrowButton(WebDriver driver) {
        this.driver = driver;
    }

    @Override
    public SecondPostUpArrowButton clickIt() {
        //indeed, this is awful, but instability of DOM structure forces it. The alternative is Thread.sleep
        WaitForConditions.waitForElementToBePresentAndDisplayed(driver, driver.findElement(By.xpath(upVoteLocator)));
        try {
            WaitForConditions.waitForToBeNonStale(driver, driver.findElement(By.xpath(upVoteLocator)));
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(upVoteLocator)));
        } catch (StaleElementReferenceException e) { }

        try {
            WaitForConditions.waitForToBeNonStale(driver, driver.findElement(By.xpath(upVoteLocator)));
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(upVoteLocator)));
        } catch (StaleElementReferenceException | TimeoutException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(upVoteLocator)));
        }
        try {
            WaitForConditions.hasAttributeChanged(driver, driver.findElement(By.xpath(upVoteLocator)), "aria-pressed", "true");
        } catch (TimeoutException e) {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", driver.findElement(By.xpath(upVoteLocator)));
            WaitForConditions.hasAttributeChanged(driver, driver.findElement(By.xpath(upVoteLocator)), "aria-pressed", "true");
        }

        return this;
    }

    @Override
    public Boolean isLoaded() {
        return driver.findElement(By.xpath("(//div[contains(@class, 'scrollerItem')]/div/div/button[@data-click-id='upvote'])[2]")).isDisplayed();
    }

    public Boolean beenHasChosen() {
        return driver.findElement(By.xpath(upVoteLocator)).getAttribute("aria-pressed").equals("true");
    }
}
