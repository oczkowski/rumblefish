package elements;

import elements.global.Add;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/7/18.
 */
public class Popular implements Add {

    private WebDriver driver;
    private WebElement element;

    public Popular(WebDriver driver) {
        this.driver = driver;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.cssSelector("div:nth-of-type(1) > div > " +
                "div:nth-of-type(2) > div > div > div:nth-of-type(2) > div > div > div > div:nth-of-type(2) > div:nth-of-type(3) >" +
                " div:nth-of-type(2) > div > div:nth-of-type(1)"));
    }

    @Override
    public Boolean isLoaded() {
        return element.isDisplayed();
    }
}
