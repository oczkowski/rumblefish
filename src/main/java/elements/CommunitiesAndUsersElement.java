package elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import tools.WaitForConditions;

/**
 * Created by kuba on 10/10/18.
 */
public class CommunitiesAndUsersElement {

    private WebDriver driver;
    private WebElement element;
    private String subredditName;

    public CommunitiesAndUsersElement(WebDriver driver, String subredditName) {
        this.driver = driver;
        this.subredditName = subredditName;
        element = WaitForConditions.waitForElementToBePresentAndDisplayed(driver, By.xpath("//div[span[text()='Communities and users']]//a[@href='/r/"+ subredditName + "/']"));
    }

    public CommunitiesAndUsersElement clickIt() {
        element.click();
        return this;
    }
}
