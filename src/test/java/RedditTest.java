import org.testng.annotations.Test;

/**
 * Created by kuba on 10/8/18.
 */
public class RedditTest extends TestBase {


    @Test
    public void searchForEthereumSubredditLoginAndVoteTest() {
        given
                .pageLoaded("http://www.reddit.com");

        when
                .searchForSubredditPage("ethereum")
                .openLocalizedSubredditPage("ethereum")
                .printOutTopMostTitle()
                .loginAsUser("ocz_", "ocz123")
                .voteToTheSecondPost();
        then
                .upOrDownArrowButtonShouldBeChosen();
    }
}
