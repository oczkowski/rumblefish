# Reddit test scenario for Rumble Fish

## Overview
This project is a complete test scenario for Rumble Fish. It has been built with Java 8, Maven and Selenium WebDriver framework.
The whole structure is based on object-oriented programming approach and PageObject. It has been slightly modified to the needs of this project.
Page models invoke element driven objects that contain all of useful navigation methods like clicking, entering string chains etc.
It offers complete reusage and universality of created implementations. Additionally, pseudo-BDD layer has been provided, that gives very clear and readable output.

## Technical requirements
To run the project you will need Java 8 JDK and Apache Maven (v. 3.0.5 or later) installed. This suite is configured to work under Linux 64-bit machines. To run it under other OSs, please follow the steps below.
Base configuration is very minimal and supports Firefox browser only.
Other dependencies (Selenium, TestNG) will be collected by Maven during first run.

## Running on Linux
### Running with Maven
After cloning the repository (```git clone https://oczkowski@bitbucket.org/oczkowski/rumblefish.git```) open shell terminal and navigate to main project directory containing pom.xml file. To check if the suite starts, please simply enter following command:
```
mvn clean test
```
If you are a 64-bit Linux version user, the project should be successfully compiled and then Firefox browser should be launched. Gecko driver has been attached to the project and should be applicable for contemporary Firefox editions.
In case of any Gecko driver issues, you can use your own release by replacing existing one (src/main/resources) or using -Dwebdriver.gecko.driver flag with Maven:
```
mvn clean test -Dwebdriver.gecko.driver={location}
```
### Running with INtelliJ
Import project to Intellij and redirect to src/test/java/RedditTest. Do a right-click on @Test annotation and choose Run (TestNG plugin needed).

## Running on Win/Mac
### Running with Maven
Follow the instructions for Linux users. The only difference is a must to replace existing Gecko driver with compatible one.
You can do it by dropping Gecko driver into src/main/resources or pointing exact location manually with -Dwebdriver.gecko.driver flag:
```
mvn clean test -Dwebdriver.gecko.driver={location}
```
### Running with INtelliJ
The procedure is exactly the same as Linux-related instruction.

